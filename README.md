Based on the [Paper Icon Theme](https://github.com/snwh/paper-icon-theme/tree/master/src/cursors) by Sam Hewitt.

"[Paper Icons](http://snwh.org/paper/icons)" by [Sam Hewitt](http://samuelhewitt.com/) is licensed under [CC-SA-4.0](http://creativecommons.org/licenses/by-sa/4.0/)

Latest Version: [Download](https://gitlab.com/skylermoore/paper-nord-cursors/-/jobs/artifacts/master/download?job=render)

Quick Install:

```
curl -L https://gitlab.com/skylermoore/paper-nord-cursors/-/jobs/artifacts/master/download?job=render -o paper-nord-cursors.zip
unzip paper-nord-cursors.zip -d paper-nord-cursors
cd paper-nord-cursors
./install.sh
```

## Cursor Theme Source

- Do not edit cursor assets directly (i.e. those in the "Paper" folder)! 
- To modify the cursors, edit source SVG file found in this directory and render them with the appropriate script.
- To edit the cursors you will need `inkscape` installed and to build and the render the cursor set you'll need `python-pil` and `x11-apps` installed.

## Render Scripts

For simplified development, has various scripts to render and build the cursor set are provided

 - [**render-cursors.py**](./render-cursors.py) will render the cursor PNG assets into [bitmaps](./bitmaps) at the appropriate sizes; ran by passing the source filename to it: `./render-cursors.py paper-nord-light-cursors.svg`
 - [**x11-make.sh**](./x11-make.sh) builds the cursor assets into a Xcursor set. This has been modified to now take a theme name as the first parameter: `./x11-make.sh paper-nord-light-cursors`
 - [**w32-make.sh**](./w32-make.sh) builds the cursor assets into a Windows cursor set.

 The following are added scripts to make working with the Nord version simpler:

 - [**build.sh**](./build.sh) will build the Light and Dark version of the cursor by calling the appropriate scripts for x11: `./build.sh`
 - [**install.sh**](./install.sh) installs the cursors to the $HOME/.icons directory
 - [**uninstall.sh**](./uninstall.sh) uninstalls the cursors from the $HOME/.icons directory

## Cursor SVG source

The source SVGs for the cursors are laid out in such a way to make editing/creating cursors simple, with a variety of layers:

 - `hotspots` shows the exact point where the cursor is active within the 24x24 region (hidden by default)
 - `slices` is read by the render script and each 24x24 square had an ID that is the cursor file name (hidden by default)
 - `cursors` pretty self-explanatory, these are the drawn cursors
 - `labels` are just labels

Both the busy cursors (the large and pointer versions) require 60 different assets to achieve a 60 FPS animation when compiled (each variant is a 6&deg; rotation of the busy indicator).
