if [ ! -d "$HOME/.icons" ]; then
	mkdir "$HOME/.icons"
fi

if [ -d "build/Paper-Nord-Light-Cursors" ]; then
	cp -r build/Paper-Nord-Light-Cursors "$HOME/.icons"
fi

if [ -d "build/Paper-Nord-Dark-Cursors" ]; then
	cp -r build/Paper-Nord-Dark-Cursors "$HOME/.icons"
fi
