clean_bitmaps() {
	if [ -d "bitmaps/24x24" ]; then
		rm -r "bitmaps/24x24"
	fi

	if [ -d "bitmaps/32x32" ]; then
		rm -r "bitmaps/32x32"
	fi

	if [ -d "bitmaps/48x48" ]; then
		rm -r "bitmaps/48x48"
	fi
}

build_light() {
	echo "Cleaning bitmap folder"
	clean_bitmaps
	echo "Rendering light cursors"
	./render-cursors.py paper-nord-light-cursors.svg
	echo "Making x11 theme"
	./x11-make.sh "Paper-Nord-Light-Cursors"
}

build_dark() {
	echo "Cleaning bitmap folder"
	clean_bitmaps
	echo "Rendering dark cursors"
	./render-cursors.py paper-nord-dark-cursors.svg
	echo "Making x11 theme"
	./x11-make.sh "Paper-Nord-Dark-Cursors"
}

build_light
build_dark
