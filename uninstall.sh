if [ -d "$HOME/.icons/Paper-Nord-Light-Cursors" ]; then
	rm -r "$HOME/.icons/Paper-Nord-Light-Cursors"
fi

if [ -d "$HOME/.icons/Paper-Nord-Dark-Cursors" ]; then
	rm -r "$HOME/.icons/Paper-Nord-Dark-Cursors"
fi
